# frozen_string_literal: true

require 'monad_router/rack'
require 'rack'
require 'rack/test'

RSpec.describe MonadRouter::Rack::Middleware do
  include Rack::Test::Methods
  let(:options) { Hash[] }

  let(:app) do
    described_class = self.described_class
    options = self.options

    Rack::Builder.app do
      use described_class, options do
        msum(
          match('/foo') >> msum(
            get('/bar') >> pure([:foo, :bar]),
            get(/\d+/) >= ->(id) { pure [:foo, :show, Integer(id)] },
            post('/') >> pure([:foo, :create])
          ),
          match('categories') >> match(/.+/) >= (
            lambda do |category_name|
              msum(
                get('/') >>
                  pure([:articles, :index, category_name: category_name]),
                get(/.+/) >= (
                  lambda do |id|
                    pure(
                      [:articles, :show, category_name: category_name, id: id]
                    )
                  end
                )
              )
            end
          ),
          match('bat') >> ((get('/') >> pure) | get(/.+/)) >= (
            ->(id) { get('/') >> pure(id ? [:bat, :show, id] : [:bat, :index]) }
          ),
          pure([:not_found])
        )
      end

      run ->(env) { [200, {}, JSON.dump(env)] }
    end
  end

  def execute(method, *args)
    JSON.parse(public_send(method, *args).body)
  end

  describe 'routing' do
    # rubocop:disable Style/WordArray
    routing_table = {
      [:get, '/foo/bar?baz=456'] => ['foo', 'bar'],
      [:get, '/foo/bar/baz/456'] => ['not_found'],
      [:post, '/foo/bar'] => ['not_found'],
      [:get, '/foo/baz'] => ['not_found'],
      [:get, '/foo/322'] => ['foo', 'show', 322],
      [:post, '/foo'] => ['foo', 'create'],
      [:get, '/categories'] => ['not_found'],
      [:get, '/categories/foo'] =>
        ['articles', 'index', 'category_name' => 'foo'],
      [:get, '/categories/foo/bar'] =>
        ['articles', 'show', 'category_name' => 'foo', 'id' => 'bar'],
      [:get, '/bat'] => ['bat', 'index'],
      [:get, '/bat/'] => ['bat', 'index'],
      [:get, '/bat/zap'] => ['bat', 'show', 'zap'],
      [:get, '/bat/zap/'] => ['bat', 'show', 'zap']
    }
    # rubocop:enable Style/WordArray

    routing_table.each do |(method, path), target|
      it "routes #{method.to_s.upcase} #{path} to #{target.inspect}" do
        expect(execute(method, path)['route']).to match(target)
      end
    end
  end

  describe 'options' do
    subject(:env) { execute :get, '/foo/bar' }

    describe ':read_request_from' do
      alias_method :old_app, :app

      let(:request) { double(:request, method: :get, path: %w[foo bar]) }
      let(:options) { Hash[read_request_from: :request] }

      let(:app) do
        request = self.request
        old_app = self.old_app

        middleware = Class.new do
          def initialize(app)
            @app = app
          end

          define_method(:call) do |env|
            env[:request] = request
            @app.call(env)
          end
        end

        Rack::Builder.app do
          use middleware
          run old_app
        end
      end

      it 'reads request from env' do
        expect(execute(:put, '/bat')['route']).to eq %w[foo bar]
      end
    end

    describe ':write_request_to' do
      let(:options) { Hash[write_request_to: :request] }

      it 'writes to env' do
        expect(execute(:get, '/foo/bar/')['request'])\
          .to match(/MonadRouter::Rack::Request/)
      end
    end

    describe ':write_route_to' do
      let(:options) { Hash[write_route_to: :secret] }

      it 'changes field name' do
        expect(execute(:get, '/foo/bar/'))
          .to include('secret' => %w[foo bar])
          .and not_include('route')
      end
    end
  end
end
