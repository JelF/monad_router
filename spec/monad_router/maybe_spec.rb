# frozen_string_literal: true

require 'support/monad_laws'
require 'monad_router/maybe'

RSpec.describe MonadRouter::Maybe do
  include_context 'monad laws'

  it_behaves_like 'Functor'
  it_behaves_like 'Applicative Functor'
  it_behaves_like 'Alternative'
  it_behaves_like 'Monad'

  describe '#value' do
    context 'when present' do
      subject(:maybe) { pure 'test' }

      it 'returns test' do
        expect(maybe.value).to eq 'test'
      end
    end

    context 'when empty' do
      subject(:maybe) { empty }

      it 'raises MonadRouter::Maybe::Empty' do
        expect { maybe.value }
          .to raise_error(MonadRouter::Maybe::Empty)
      end
    end
  end

  describe 'equality' do
    it 'pure foo == pure foo' do
      expect(pure('foo')).to eq pure('foo')
    end

    it 'pure foo != pure bar' do
      expect(pure('foo')).not_to eq pure('bar')
    end

    it 'pure foo != empty' do
      expect(pure('foo')).not_to eq empty
    end

    it 'empty == empty' do
      expect(empty).to eq empty
    end
  end
end
