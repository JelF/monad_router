# frozen_string_literal: true

require 'support/monad_laws'
require 'support/router_context_and_profunctor_laws'
require 'monad_router/http_request_router'

RSpec.describe MonadRouter::HTTPRequestRouter do
  include_context 'monad laws'
  include_context 'router'

  it_behaves_like 'Functor'
  it_behaves_like 'Applicative Functor'
  it_behaves_like 'Alternative'
  it_behaves_like 'Monad'
  it_behaves_like 'Contrafunctor'
  it_behaves_like 'Router'

  describe '#exec' do
    def stub_request(value, method = :get)
      double(:request, path: value, method: method)
    end

    shared_examples 'with test and test_with_proxy' do
      context 'when test get truthy' do
        it 'successes' do
          expect { router.exec(stub_request(true)) }
            .not_to raise_error
        end
      end

      context 'when test get falsey' do
        it 'successes' do
          expect { router.exec(stub_request(false)) }
            .to raise_error(MonadRouter::Router::RouteNotFound)
        end
      end
    end

    context 'with test' do
      let(:router) do
        described_class.test(&:path)
      end

      it_behaves_like 'with test and test_with_proxy'

      context 'with contramap' do
        it 'ignores contramap' do
          contramaped_router = router.contramap_request_path(&:!)
          expect { contramaped_router.exec(stub_request(true)) }
            .not_to raise_error
        end
      end
    end

    context 'with test_with_proxy' do
      let(:router) do
        described_class.test_with_proxy(&:path)
      end

      it_behaves_like 'with test and test_with_proxy'

      context 'with contramaped before test' do
        let(:router) do
          pure('foo').contramap_request_path(&:!) >>
            described_class.test_with_proxy(&:path)
        end

        it 'uses updated route' do
          expect { router.exec(stub_request(false)) }.not_to raise_error
        end
      end

      context 'with contramaped after test' do
        let(:router) do
          described_class.test_with_proxy(&:path) >>
            pure('foo').contramap_request_path(&:!)
        end

        it 'uses old route' do
          expect(router.exec(stub_request(true))).to eq 'foo'
        end
      end
    end

    shared_examples 'with test_maybe and test_maybe_with_proxy' do
      context 'when test get pure' do
        it 'successes' do
          expect { router.exec(stub_request(MonadRouter::Maybe.pure)) }
            .not_to raise_error
        end
      end

      context 'when test get empty' do
        it 'fails' do
          expect { router.exec(stub_request(MonadRouter::Maybe.empty)) }
            .to raise_error(MonadRouter::Router::RouteNotFound)
        end
      end
    end

    context 'with test_maybe' do
      let(:router) { described_class.test_maybe(&:path) }

      it_behaves_like 'with test_maybe and test_maybe_with_proxy'
    end

    context 'with test_maybe_with_proxy' do
      let(:router) { described_class.test_maybe_with_proxy(&:path) }

      it_behaves_like 'with test_maybe and test_maybe_with_proxy'
    end

    context 'with exact' do
      let(:router) { described_class.exact }

      context 'when path is empty' do
        it 'successes' do
          expect { router.exec(stub_request([])) }
            .not_to raise_error
        end
      end

      context 'when path is present' do
        it 'fails' do
          expect { router.exec(stub_request(%w[foo])) }
            .to raise_error(MonadRouter::Router::RouteNotFound)
        end
      end
    end

    context 'with match string' do
      let(:router) { described_class.match('foo') }

      context 'when matched' do
        it 'successes' do
          expect { router.exec(stub_request(%w[foo bar])) }.not_to raise_error
        end
      end

      context 'when failed' do
        it 'fails' do
          expect { router.exec(stub_request(%w[bar])) }
            .to raise_error(MonadRouter::Router::RouteNotFound)
        end
      end
    end

    context 'with match regexp' do
      let(:router) { described_class.match(/f.+/) }

      context 'when matched regexp without groups' do
        it 'successes' do
          expect(router.exec(stub_request(%w[foo]))).to eq 'foo'
        end
      end

      context 'when matched regexp with one group' do
        let(:router) { described_class.match(/f(.+)/) }

        it 'successes' do
          expect(router.exec(stub_request(%w[foo]))).to eq 'oo'
        end
      end

      context 'when matched regexp with many groups' do
        let(:router) { described_class.match(/(f)(.+)/) }

        it 'successes' do
          expect(router.exec(stub_request(%w[foo]))).to eq %w[f oo]
        end
      end

      context 'when failed' do
        it 'fails' do
          expect { router.exec(stub_request(%w[bar])) }
            .to raise_error(MonadRouter::Router::RouteNotFound)
        end
      end
    end

    http_methods = %i[get post put patch delete link unlink]
    http_methods.each do |method|
      context "with #{method}" do
        let(:router) { described_class.public_send(method, 'foo') }

        context 'when matched' do
          it 'successes' do
            expect { router.exec(stub_request(%w[foo], method)) }
              .not_to raise_error
          end
        end

        context 'when failed' do
          it 'fails' do
            another_method = http_methods.find { |x| x != method }
            expect { router.exec(stub_request(%w[foo], another_method)) }
              .to raise_error(MonadRouter::Router::RouteNotFound)
          end
        end
      end
    end

    context 'with .draw complicated request' do
      subject(:router) do
        described_class.draw do
          msum(
            match('foo') >> msum(
              get('bar') >> pure([:foo, :bar]),
              get(/\d+/) >= ->(id) { pure([:foo, :show, id: Integer(id)]) },
              post('/') >> pure([:foo, :create])
            ),
            get('bar') >> pure([:bar])
          )
        end
      end

      routing_table = {
        [:get, %w[foo bar]] => [:foo, :bar],
        [:get, %w[foo 123]] => [:foo, :show, id: 123],
        [:post, %w[foo bar]] => nil,
        [:get, %w[foo baz]] => nil,
        [:post, %w[foo]] => [:foo, :create],
        [:get, []] => nil,
        [:get, %w[bar]] => [:bar]
      }

      routing_table.each do |(method, path), route|
        context "#{method.to_s.upcase} /#{path.join('/')}" do
          if route
            it "routes to #{route.inspect}" do
              expect(router.exec(stub_request(path, method))).to eq route
            end
          else
            it 'could not be routed' do
              expect { router.exec(stub_request(path, method)) }
                .to raise_error(MonadRouter::Router::RouteNotFound)
            end
          end
        end
      end
    end
  end
end
