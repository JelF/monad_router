# frozen_string_literal: true

require 'support/monad_laws'
require 'support/router_context_and_profunctor_laws'
require 'monad_router/router'

RSpec.describe MonadRouter::Router do
  include_context 'monad laws'
  include_context 'router'

  it_behaves_like 'Functor'
  it_behaves_like 'Applicative Functor'
  it_behaves_like 'Alternative'
  it_behaves_like 'Monad'
  it_behaves_like 'Contrafunctor'
  it_behaves_like 'Router'

  describe '#exec' do
    context 'with test' do
      let(:router) { described_class.test(&:itself) }

      context 'when test returns true' do
        it 'matches route' do
          expect { router.exec(true) }.not_to raise_error
        end
      end

      context 'when test returns false' do
        it 'does not match route' do
          expect { router.exec(false) }
            .to raise_error(MonadRouter::Router::RouteNotFound)
        end
      end

      context 'when contramaped before test' do
        let(:router) do
          pure('foo').contramap(&:!) >> described_class.test(&:itself)
        end

        it 'uses updated route' do
          expect { router.exec(false) }.not_to raise_error
        end
      end

      context 'when contramaped after test' do
        let(:router) do
          described_class.test(&:itself) >> pure('foo').contramap(&:!)
        end

        it 'uses old route' do
          expect(router.exec(true)).to eq 'foo'
        end
      end
    end

    context 'with test_maybe' do
      let(:router) { described_class.test_maybe(&:itself) }

      context 'when test returns value' do
        it 'matches route' do
          expect(router.exec(MonadRouter::Maybe.pure('test'))).to eq 'test'
        end
      end

      context 'when test returns false' do
        it 'does not matche route' do
          expect { router.exec(MonadRouter::Maybe.empty) }
            .to raise_error(MonadRouter::Router::RouteNotFound)
        end
      end
    end

    context 'with complicated router' do
      # NOTE: pure has no much helpers so we have to do much work to build it
      let(:router) do
        (
          described_class.test { |request| request[0] == 'foo' } >> (
            pure(nil).contramap { |request| request[1..-1] } >> (
              (
                described_class.test { |request| request == %w[bar] } >>
                  pure('baz')
              ) | (
                (
                  described_class.test_maybe do |request|
                    if request.length == 1
                      MonadRouter::Maybe.pure request[0]
                    else
                      MonadRouter::Maybe.empty
                    end
                  end
                ) >= ->(x) { pure x }
              )
            )
          )
        ) | (
          described_class.test { |request| request[0] == 'bat' } >>
            pure('bat')
        )
      end

      routing_table = {
        %w[foo] => nil,
        %w[foo bar] => 'baz',
        %w[foo baz] => 'baz',
        %w[foo bat] => 'bat',
        %w[foo bar bax] => nil,
        %w[foo baz bax] => nil,
        %w[bat] => 'bat',
        %w[bat bart] => 'bat'
      }

      routing_table.each do |request, expectation|
        if expectation
          it "routes #{request} to #{expectation}" do
            expect(router.exec(request)).to eq expectation
          end
        else
          it "fails to route #{request}" do
            expect { router.exec(request) }
              .to raise_error MonadRouter::Router::RouteNotFound
          end
        end
      end
    end
  end
end
