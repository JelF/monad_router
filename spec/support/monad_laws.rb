# frozen_string_literal: true

RSpec.shared_context 'monad laws' do # rubocop:disable RSpec/ContextWording
  def expect_equal(left, right)
    expect(left).to eq(right)
  end

  def empty
    described_class.empty
  end

  def pure(value)
    described_class.pure(value)
  end

  shared_examples 'Functor' do
    subject(:functor) { pure '' }

    it 'satisfies identity law' do
      expect_equal(functor.fmap(&:itself), functor)
    end

    it 'satisfies composition law' do
      expect_equal(
        functor.fmap { |x| x + 'foo' }.fmap { |x| x + 'bar' },
        functor.fmap { |x| x + 'foobar' }
      )
    end
  end

  shared_examples 'Applicative Functor' do
    it 'satisfies composition law' do
      f = pure ->(x) { x + 'foo' }
      g = pure ->(x) { x + 'bar' }
      x = pure ''
      compose = pure(->(u) { ->(v) { ->(t) { u.call(v.call(t)) } } })

      expect_equal(compose.ap(f).ap(g).ap(x), f.ap(g.ap(x)))
    end

    it 'satisfies homomorphism law' do
      f = ->(x) { x + 'bar' }
      x = 'foo'
      expect_equal(pure(f).ap(pure(x)), pure(f.call(x)))
    end

    it 'satisfies interchange law' do
      f = pure ->(x) { x + 'bar' }
      x = 'foo'
      expect_equal(f.ap(pure(x)), pure(->(u) { u.call(x) }).ap(f))
    end
  end

  shared_examples 'Alternative' do
    it 'satisfies empty neiutrality law' do
      x = pure 'foo'
      expect_equal(empty | x, x)
      expect_equal(x | empty, x)
    end

    it 'satisfies association law' do
      x = pure 'foo'
      y = pure 'bar'
      z = pure 'baz'

      expect_equal(x | (y | z), (x | y) | z)
      expect_equal(empty | (y | z), (empty | y) | z)
      expect_equal(x | (empty | z), (x | empty) | z)
      expect_equal(empty | (empty | z), (empty | empty) | z)
    end
  end

  shared_examples 'Monad' do
    it 'satisfies left identity law' do
      x = 'foo'
      f = ->(t) { pure(t + 'bar') }
      expect_equal(pure(x) >= f, f.call(x))
    end

    it 'satisfies right identity law' do
      x = pure 'foo'
      expect_equal(x >= ->(t) { pure(t) }, x)
    end

    it 'satisfies associativity law' do
      x = pure 'foo'
      f = ->(t) { pure(t + 'foo') }
      g = ->(t) { pure(t + 'bar') }
      expect_equal((x >= f) >= g, x >= ->(t) { f.call(t) >= g })
    end
  end
end
