# frozen_string_literal: true

RSpec.shared_context 'router' do # rubocop:disable RSpec/ContextWording
  def expect_equal(left, right)
    expect(left.exec(request)).to eq(right.exec(request))
  end

  def update_path(request, new_segments)
    request.dup.tap { |x| x.path += new_segments }
  end

  let(:request) { Struct.new(:path).new(%w[test]) }

  shared_examples 'Contrafunctor' do
    subject(:contrafunctor) do
      described_class.test_maybe { |x| MonadRouter::Maybe.pure x }
    end

    it 'satisfies identity law' do
      expect_equal(contrafunctor.contramap(&:itself), contrafunctor)
    end

    it 'satisfies contrafunctor composition law' do
      expect_equal(
        contrafunctor
          .contramap { |x| update_path(x, %w[bar]) }
          .contramap { |x| update_path(x, %w[foo]) },
        contrafunctor.contramap { |x| update_path(x, %w[foo bar]) }
      )
    end
  end

  shared_examples 'Router' do
    describe '#exec' do
      context 'with pure value' do
        it 'returns that value' do
          expect(pure('test').exec(request)).to eq 'test'
        end
      end

      context 'with failed router' do
        it 'raises routing error' do
          expect { empty.exec(request) }
            .to raise_error(MonadRouter::Router::RouteNotFound)
        end
      end

      context 'with sum of routers' do
        context 'when first route not fail' do
          let(:router) { pure('foo') | pure('bar') }

          it 'returns first route result' do
            expect(router.exec(request)).to eq 'foo'
          end
        end

        context 'when first route fail' do
          let(:router) { empty | pure('bar') }

          it 'returns first route result' do
            expect(router.exec(request)).to eq 'bar'
          end
        end
      end
    end
  end
end
