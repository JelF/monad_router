# frozen_string_literal: true

require 'monad_router/maybe'
require 'monad_router/monad'
require 'monad_router/router'
require 'monad_router/http_request_router'
require 'monad_router/version'

# Collection of monads to route http requests
module MonadRouter
  # Raised when #exec on Router does not find a way to resolve it.
  # To prevent this, you can add something like `| handle_not_found`
  # to the end of router defenition
  RouteNotFound = Router::RouteNotFound
end
