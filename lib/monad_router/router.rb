# frozen_string_literal: true

require 'monad_router/maybe'
require 'monad_router/monad'

module MonadRouter
  # Simple router without binding to http requests.
  # You can extend it to use within non-rest services
  # i.e. rabbitMQ listener
  class Router < Monad
    # Raised when #exec on Router does not find a way to resolve it.
    # To prevent this, you can add something like `| handle_not_found`
    # to the end of router defenition
    RouteNotFound = Class.new(StandardError)

    # Container for request and value
    ValueWithRequest = Struct.new(:value, :request)

    class << self
      # @!method self.new(resolver)
      #   @api private
      #   @see #initialize

      # pure in terms of router means exactly this value would be returned by
      # exec. It will not depend on request so you can add it to the end
      # of chain and return something usefull (handler, proc or anything else
      # which will be used after routing is done).
      # @param value Generic value
      # @return [Router<value>]
      def pure(value = nil)
        _test_maybe { Maybe.pure value }
      end

      # Failure constructor which will be skiped in '|' composition.
      # It is usefull to make your own test failed monads, but I suggest
      # to use test_maybe instead.
      # @return [Router]
      def empty
        _test_maybe { Maybe.empty }
      end

      # test_maybe is core of any route checks. If yielded block returns
      # Maybe.pure it will make Router with this value and otherwise
      # it will make a failed router.
      # @yield
      #   request which can be analized to make a decision.
      #   Note that request can be modified between exec and this check.
      # @return [Router]
      def test_maybe
        resolver = lambda do |request|
          yield(request) >= (
            lambda do |result|
              Maybe.pure ValueWithRequest.new(result, request)
            end
          )
        end

        new resolver
      end

      # test is a test_maybe wich throws away value. It will fail router if
      # yield result is falsey
      # @yield request which can be analized to make a decision
      # @return [Router]
      # @see .test_maybe
      def test
        _test_maybe { |request| yield(request) ? Maybe.pure : Maybe.empty }
      end

      # @api private
      alias _test test

      # @api private
      alias _test_maybe test_maybe
    end

    # @api private
    # @param resolver [Proc<Object => MonadRouter::Maybe<ValueWithRequest>>]
    # @see #resolver
    def initialize(resolver)
      @resolver = resolver
    end

    # Alternative duck type
    # (see it's laws in spec/support/monad_laws.rb)
    # For routers we can call it more like sum. This combinator
    # allows to combine routers to try second if first failed
    # @param other [MonadRouter::Router]
    # @return [MonadRouter::Router]
    def |(other)
      new_resolver = lambda do |request|
        resolver.call(request) | other.resolver.call(request)
      end

      self.class.new(new_resolver)
    end

    # This combinator can be used to chain conditions in router.
    # I.e. you can use test { |request| request == '/foo' } >> pure(FooHandler)
    # If you want to get information how to use it,
    # see MonadRouter::HTTPRequestRouter or README.md to get examples
    # @see #resolver
    # @see MonadRouter::HTTPRequestRouter
    # @see README.md
    # @param router_resolver [Proc<Object => MonadRouter::Router>]
    # @return [MonadRouter::Router]
    def >=(router_resolver)
      new_resolver = lambda do |request|
        resolver.call(request) >= (
          lambda do |value_with_request|
            new_router = router_resolver.call(value_with_request.value)
            new_router.resolver.call(value_with_request.request)
          end
        )
      end

      self.class.new(new_resolver)
    end

    # Extracts stored value from router. This obliviously requires to pass
    # request
    # @param request
    #   Router accepts generic requests but as a result there are no way
    #   to make much helpers and router building is a problem.
    # @return stored value
    # @raise [MonadRouter::Router::RouteNotFound]
    # @see MonadRouter::HTTPRequestRouter#exec
    def exec(request)
      result = resolver.call(request)
      result.present? ? result.value.value : raise(RouteNotFound)
    end

    # Contramap is like a fmap but reverse. It is a part of Contrafunctor and
    # Bifunctor duck types. Contramap transforms request
    # before it will passed to resolver. When two Routers combined,
    # second router will also be affected by transformation in first,
    # (This is semantic is known for State monad).
    # @see resolver
    #   resolver attribute for more info about how it linked to State monad
    def contramap
      self.class.new ->(request) { resolver.call(yield(request)) }
    end

    protected

    # Resolver logic is a bit complicated. Entrire MonadRouter::Router
    # is a composition of state and maybe monads.
    # From state resolver takes it's form Object => ValueWithRequest
    # which allows it to both read and modify request value when cahined
    # with >=. Maybe injected in the middle
    # (it could be injected inside ValueWithRequest, but it will make code less
    # simple) to add it's Alternative semantics.
    # @return [Proc<Object => MonadRouter::Maybe<ValueWithRequest>>]
    attr_reader :resolver
  end
end
