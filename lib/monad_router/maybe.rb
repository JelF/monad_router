# frozen_string_literal: true

require 'monad_router/monad'

module MonadRouter
  # Maybe monad is like list with zero or one argument.
  # We use it to guarantee nill values will be truthy when
  # we pass them as a value, not as absence of value
  class Maybe < Monad
    # Error which raised when you try to get empty Maybe value
    Empty = Class.new(StandardError)

    # @!method self.new(value, is_present)
    #  @api private
    #  @see #initialize

    # Makes Maybe monad with a value. Event if value is nil,
    # it still be #present? so we can say like success(nil)
    # @param value generic value
    # @return [Maybe<value>]
    def self.pure(value = nil)
      new(value, true)
    end

    # Makes Maybe without a value. It will not be #present?
    # and represents failed attempts. I.e. in
    # MonadRouter::Router.monad_test it will mean than test was
    # failed and route does not match
    # @return [Maybe]
    def self.empty
      new(nil, false)
    end

    # @param value generic value to store
    # @param is_present chooses between present and not
    # @api private
    def initialize(value, is_present)
      @value = value
      @is_present = is_present
    end

    # Stored value. You should use present? check before you call it
    # or it will raise
    # @raise [Empty] unless present?
    # @return stored value
    def value
      raise Empty unless present?
      @value
    end

    # @return [Boolean] true if maybe present
    # @return [Boolean] false otherwise
    def present?
      @is_present
    end

    # Alternative duck type
    # (see it's laws in spec/support/monad_laws.rb)
    # It works like || for ruby types, hovewer it does computation
    # for second param.
    # @param [Maybe] other
    # @return [Maybe] itself if it is present
    # @return [Maybe] other otherwise
    def |(other)
      present? ? self : other
    end

    # Maybe monad combinator. If it is present throws value and
    # computes result, otherwise returns empty
    # @param [Proc<anything => Maybe>] resolver
    # @return [Maybe] resolver.call(value) if present
    # @return [Maybe] .empty otherwise
    def >=(resolver)
      present? ? resolver.call(value) : self.class.empty
    end

    # Equality check overload
    # @param [Maybe] other
    # @return [Boolean] true if both are present and values equal
    # @return [Boolean] true if both not present
    # @return [Boolean] false otherwise
    def ==(other)
      return false unless other.is_a?(Maybe)

      return value == other.value if present? && other.present?
      return true if !present? && !other.present?
      false
    end
  end
end
