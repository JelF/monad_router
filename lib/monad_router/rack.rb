# frozen_string_literal: true

require 'monad_router/rack/middleware'

module MonadRouter
  # Rack middleware, request adapter and other stuff
  module Rack
  end
end
