# frozen_string_literal: true

require 'monad_router/router'
require 'monad_router/http_request_router/helpers'
require 'monad_router/http_request_router/proxy'

module MonadRouter
  # Router for http requests with duck type #path #method
  class HTTPRequestRouter < Router
    # Special proxy to handle path changes during processing as long as
    # original request. match will change path but not change original.path
    # @!attribute path
    #   @return [Array<String>] possible changed path
    # @!attribute original
    #   @return [#path #method]
    #     original request send to #exec (without path changed)
    RequestProxy = Struct.new(:original, :path)

    class << self
      # @!method test_maybe_with_proxy
      # @yield [RequestProxy]
      # @return [HTTPRequestRouter]
      # @see MonadRouter::Request.test_maybe
      alias test_maybe_with_proxy test_maybe

      # @!method test_with_proxy
      # @yield [RequestProxy]
      # @return [HTTPRequestRouter]
      # @see MonadRouter::Request.test
      alias test_with_proxy test

      # @yield original request passed to exec (without path changes)
      # @return [HTTPRequestRouter]
      # @see MonadRouter::Request.test
      def test
        super { |proxy| yield(proxy.original) }
      end

      # @yield original request passed to exec (without path changes)
      # @return [HTTPRequestRouter]
      # @see MonadRouter::Request.test_maybe
      def test_maybe
        super { |proxy| yield(proxy.original) }
      end

      include Helpers

      # Same as instance_exec but inside proxy object so you can use
      # things like extend without changing router global
      # @param args args to be yielded
      # @yield given args
      def draw(*args, &block)
        Proxy.new.instance_exec(*args, &block)
      end
    end

    # executes router with speacial duck-typed request
    # @param request [#path => Array<String>, #method => Symbol]
    # @return execution result
    # @see MonadRouter::Router#exec
    def exec(request)
      super(RequestProxy.new(request, request.path))
    end

    # It is not a contramap, just a helper to do it with proxy path field
    # @return [HTTPRequestRouter]
    def contramap_request_path
      contramap do |request|
        RequestProxy.new(request.original, yield(request.path))
      end
    end
  end
end
