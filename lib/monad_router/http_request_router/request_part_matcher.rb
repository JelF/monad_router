# frozen_string_literal: true

require 'monad_router/router'

module MonadRouter
  class HTTPRequestRouter < Router
    # Checks if request part matches some expression passed by user
    # @api private
    class RequestPartMatcher
      # @param matcher [String, Regexp, nil]
      def initialize(matcher)
        @matcher = normalize_matcher(matcher)
      end

      # @param request_part [String]
      # @return [MonadRouter::Maybe]
      def match(request_part)
        return request_part.nil? ? Maybe.pure : Maybe.empty if @matcher.nil?
        return Maybe.empty unless (match_results = @matcher.match(request_part))

        case match_results.size
        when 1 then Maybe.pure(match_results[0])
        when 2 then Maybe.pure(match_results[1])
        else Maybe.pure(match_results[1..-1])
        end
      end

      private

      # @param matcher [String, Regexp, nil]
      # @return nil
      # @return [Regexp]
      def normalize_matcher(matcher)
        matcher = matcher.sub(%r{\A/}, '') if matcher.is_a?(String)
        matcher = nil if matcher.is_a?(String) && matcher.empty?
        matcher = Regexp.escape(matcher) if matcher.is_a?(String)
        matcher = /\A#{matcher}\z/ if matcher
        matcher
      end
    end
  end
end
