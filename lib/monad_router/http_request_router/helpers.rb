# frozen_string_literal: true

require 'monad_router/router'
require 'monad_router/http_request_router/request_part_matcher'

module MonadRouter
  class HTTPRequestRouter < Router
    # Collection of HTTPRequestRouter factories
    module Helpers
      # test that all segments matched
      # @return [HTTPRequestRouter]
      def exact
        test_with_proxy { |proxy| proxy.path.empty? }
      end

      # Matches request segment and removes it from path
      # @param matcher [String, Regexp]
      # @return [HTTPRequestRouter]
      # @see match_request_part
      #   matcher processing and return value in .match_request_part
      def match(matcher)
        tested = test_maybe_with_proxy do |proxy|
          match_request_part(proxy.path[0], matcher)
        end

        tested >= (
          lambda do |match_result|
            pure(match_result).contramap_request_path do |request_path|
              request_path[1..-1].to_a
            end
          end
        )
      end

      # Match segments when request method is get
      # @param matcher [String, Regexp]
      # @return [HTTPRequestRouter]
      # @see .match
      def get(matcher)
        exact_match_with_method(:get, matcher)
      end

      # Match segments when request method is post
      # @param matcher [String, Regexp]
      # @return [HTTPRequestRouter]
      # @see .match
      def post(matcher)
        exact_match_with_method(:post, matcher)
      end

      # Match segments when request method is put
      # @param matcher [String, Regexp]
      # @return [HTTPRequestRouter]
      # @see .match
      def put(matcher)
        exact_match_with_method(:put, matcher)
      end

      # Match segments when request method is patch
      # @param matcher [String, Regexp]
      # @return [HTTPRequestRouter]
      # @see .match
      def patch(matcher)
        exact_match_with_method(:patch, matcher)
      end

      # Match segments when request method is delete
      # @param matcher [String, Regexp]
      # @return [HTTPRequestRouter]
      # @see .match
      def delete(matcher)
        exact_match_with_method(:delete, matcher)
      end

      # Match segments when request method is link
      # @param matcher [String, Regexp]
      # @return [HTTPRequestRouter]
      # @see .match
      def link(matcher)
        exact_match_with_method(:link, matcher)
      end

      # Match segments when request method is unlink
      # @param matcher [String, Regexp]
      # @return [HTTPRequestRouter]
      # @see .match
      def unlink(matcher)
        exact_match_with_method(:unlink, matcher)
      end

      # Sum (in terms of |) of routers
      # @param [Array<HTTPRequestRouter>] args
      # @return [HTTPRequestRouter]
      def msum(*args)
        args.reduce(empty, &:|)
      end

      private

      # Match segments with given method
      # @param method [Symbol]
      # @return [HTTPRequestRouter]
      # @param matcher [String, Regexp]
      def exact_match_with_method(method, matcher)
        test { |request| request.method == method } >>
          match(matcher) >= ->(x) { exact >> pure(x) }
      end

      # Matches given segment
      # @param matcher [String, Regexp]
      # @return [MonadRouter::Maybe<String>]
      #   exact request part if matcher is a String
      # @return [MonadRouter::Maybe<String>]
      #   exact request part if matcher is a Regexp without groups
      # @return [MonadRouter::Maybe<String>]
      #   matched group if matcher is a regexp with exaclty one group
      # @return [MonadRouter::Maybe<Array<String>>]
      #   all matched groups if matcher is a regexp with many groups
      # @see RequestPartMatcher
      def match_request_part(request_part, matcher)
        HTTPRequestRouter::RequestPartMatcher.new(matcher).match(request_part)
      end
    end
  end
end
