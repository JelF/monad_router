# frozen_string_literal: true

require 'monad_router/router'
require 'monad_router/http_request_router/helpers'

module MonadRouter
  class HTTPRequestRouter < Router
    # Special proxy for HTTPRequestRouter factories.
    # @see [MonadRouter::HTTPRequestRouter]
    #   MonadRouter::HTTPRequestRouter class methods
    class Proxy
      extend Forwardable
      include Helpers

      # Include more hepers inside proxy contents with #include
      alias include extend

      delegate(
        %i[test test_maybe test_with_proxy test_maybe_with_proxy pure empty] =>
          MonadRouter::HTTPRequestRouter
      )
    end
  end
end
