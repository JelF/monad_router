# frozen_string_literal: true

module MonadRouter
  # Semantic typed version '0.a.b.c'
  #
  # - '0' stands for BETA
  # - 'a' stands for major version, it's change would break back-compatibility
  # - 'b' stands for minor version,
  #   it's change would not break back-compatibility
  # - 'c' stands for version bumps for other reasons,
  #   it's change means nothing in api changed
  VERSION = '0.1.0.0'
end
