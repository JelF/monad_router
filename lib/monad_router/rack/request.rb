# frozen_string_literal: true

module MonadRouter
  module Rack
    # Rack env wrapper
    class Request
      attr_reader :env, :method, :path

      def initialize(env)
        @env = env
        @method = env['REQUEST_METHOD'].downcase.to_sym
        @path = env['PATH_INFO'].split('/')[1..-1]
      end
    end
  end
end
