# frozen_string_literal: true

require 'monad_router/http_request_router'
require 'monad_router/rack/request'

module MonadRouter
  module Rack
    # Use this middleware in config.ru to process routing before applciation
    # call
    class Middleware
      # @param app [Proc] application proc
      # @param options [Hash]
      # @option options :read_request_from
      #   env field where request object was placed in previous middleware.
      #   (Specify if you use other request representation library)
      # @option options :write_request_to
      #   env field where request object should be placed after processing.
      #   (As we do not mutate request, use only if you want to use our wrapper
      #   inside your app)
      # @option options :write_route_to (:route)
      #   env field where route (router execution result) should be placed
      #   after processing
      def initialize(app, options = {}, &block)
        @app = app
        @read_request_from = options[:read_request_from]
        @write_request_to = options[:write_request_to]
        @write_route_to = options.fetch(:write_route_to, :route)
        @router = MonadRouter::HTTPRequestRouter.draw(&block)
      end

      # Part of regular middleware API, we improve env and and call next app
      # @param env [Hash] request env
      def call(env)
        request =
          @read_request_from ? env[@read_request_from] : Rack::Request.new(env)

        route = @router.exec(request)

        env = env.dup
        env[@write_request_to] = request if @write_request_to
        env[@write_route_to] = route
        @app.call(env)
      end
    end
  end
end
