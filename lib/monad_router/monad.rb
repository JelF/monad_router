# frozen_string_literal: true

module MonadRouter
  # Generic monad superclass
  # Monads defined by duck-type `.pure` and `>=(->(anything) { Monad })`.
  # There is huge amount of functions defined for each monad and we will store
  # them here if they are required.
  # See shared examples inside spec/support/monad_laws.rb
  # to get laws of all monads
  class Monad
    # @!method self.pure(value)
    #   @abstract
    #   @param value generic value
    #   @return [Monad<value>]
    #     value wraped into monad.This wrap will obey some laws, i.e.
    #     in Router monad it will be returned by exec and in Maybe
    #     it is stored inside value.

    # @!method >=(other_resolver)
    #   >= is a most important method of monad, because
    #   it is used to combine monads of same time
    #   to build new cool monads. Resolver is a function
    #   so monads can depend on other monads hidden value.
    #   (I.e. when you use router
    #   you should be able to pass params inlined in url to action handler)
    #   @abstract
    #   @param other_resolver [Proc<Object => Monad>]
    #   @return [Monad]
    #     Monad returned by resolver, enchanted with context
    #     it could be resolved. This is most generic operation and it is depends
    #     on what do we want to use monad for.

    # Sometimes you don't need to extract value from old monad
    # to build new monad and combine them. In this case there is a helper.
    # @param other [Monad]
    # @return [Monad]
    def >>(other)
      self >= ->(_x) { other }
    end

    # ap is shortcat to applicate. All monads are applicative functors too
    # and sometimes it is easier to use them as applicative functors.
    # ap is like a call lifted to monad level. It only makes sence
    # if caller wraps a function (i.e. pure(-> (x) { x + 1 })).
    # @param other [Monad] container with argument to be called with
    # @return [Monad]
    def ap(other)
      self >= ->(f) { other >= ->(x) { self.class.pure(f.call(x)) } }
    end

    # fmap is like a map for colections
    # (By the way, Enumerator can be a monad with fmap = map).
    # It applies a block to a value inside container.
    # @yield stored value (it can be multiple values or nothing)
    # @return [Monad]
    def fmap
      self >= ->(x) { self.class.pure(yield(x)) }
    end
  end
end
