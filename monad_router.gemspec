# frozen_string_literal: true

lib = File.expand_path('lib', __dir__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require 'monad_router/version'

Gem::Specification.new do |spec|
  spec.name = 'monad_router'
  spec.version = MonadRouter::VERSION
  spec.authors = ['Alexander Smirnov']
  spec.email = ['jelf.personal@zoho.eu']

  spec.summary = 'Ruby http (and generic) router as a library'
  spec.description = 'See https://gitlab.com/JelF/monad_router for more info'
  spec.homepage = 'https://gitlab.com/JelF/monad_router'

  spec.files = Dir['lib/**/*.rb']
  spec.require_paths = ['lib']

  spec.add_development_dependency 'bundler', '~> 1.16'
  spec.add_development_dependency 'guard-rspec'
  spec.add_development_dependency 'guard-rubocop'
  spec.add_development_dependency 'guard-yard'
  spec.add_development_dependency 'launchy'
  spec.add_development_dependency 'pry'
  spec.add_development_dependency 'rack', ENV.fetch('RACK_VERSION', '~> 2.0')
  spec.add_development_dependency 'rack-test'
  spec.add_development_dependency 'rake', '~> 10.0'
  spec.add_development_dependency 'rspec', '~> 3.8'
  spec.add_development_dependency 'rubocop', '~> 0.58'
  spec.add_development_dependency 'rubocop-rspec', '~> 1.27'
  spec.add_development_dependency 'simplecov'
  spec.add_development_dependency 'yard', '~> 0.9.15'
end
