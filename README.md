# MonadRouter

Simple router as library. Use it independent or with rack middleware

## Installation

Add this line to your application's Gemfile:

```ruby
gem 'monad_router'
```

And then execute:

    $ bundle

Or install it yourself as:

    $ gem install monad_router

## Usage

> If you want to use router for bi-directional routing
(not only request => handler but also handler => url) you can either use
something else (if you also need to use tilt, i can suggest
[flame](https://github.com/AlexWayfer/flame) as a lightweight framework
with routing support) or extend HTTPRequestRouter to a bidirectional router.
If you implement it as monad please make PR. It should not be hard
and maybe i do it myself some day

This gem provides http request router as a monad so let's begin with what monads
are and why they are usefull for routing.

Monads theirselves are representing control flow, making an abstraction
to reduce direct flow control. They look not as simple as direct control
but greatly simplifies combining. Let's see simple non-monad router

```ruby
router = lambda do |request|
  path = request.path.to_a
  x, *path = path
  case x
  when 'blog'
    x, *path = path
    case x
    when /\A(\d+)\z/
      id =  Integer(Regexp.last_match[1])
      next [:article, id: id] if request.get? && path.empty?
    end
  end
  [:not_found]
end
```

Not simple, isn't it? There are some DSLs to improve this expirience,
i.e. in rails

```ruby
Rails.application.routes.draw do
  resource :articles, path: :blog, only: %i[show]
  # or without resource helper
  scope :blog, controller: ArticlesController do
    get ':id', action: :show
  end
end
```

Good news that with monads we can do it in almost same way without DSL usage

```ruby
h = MonadRouter::HTTPRequestRouter
h.msum(
  h.match('blog') >> h.get(/\d+/) >=
    ->(id) { h.pure([:article, id: Integer(id)]) },
  h.pure([:not_found]),
)
```

Is it really more complicated than rails way? Will it still be more complicated
after you implement resource helper?

See what was done in that example

1. we use `msum` to choose first matching route.
  `msum` appears when we have a Monad with Alternative interface (aka MonadPlus)
  it's ruby closest equivalent is `[].first(&:present?)`
1. we use `match` to match only routes started with 'blog' and remove 'blog'
  from further matching
1. we use `get` helper to ensure following:
  1. Request method is `:get`
  1. It's path is something like /\d+/
  1. Whole path already matched

  It will collect regexp match result so we can use it in next lambda
1. we use `pure` inside lambda to constuct router which always routes to
  this value. Obliviously, it only will work if all previous monads in
  chain will
1. we use `>>` and `>=` to combine monads. `>>` is just a helper

  `a >> b == a >= ->(*) { b }`

  and `>=` combines routers in an understandable (i hope) way.
  1. Lambda expression would be executed with `value` of a.
    What `value` is depends on router used, for `pure` it is guaranteed to
    be same as passed to pure, for `test` it always nil, match yields
    value based on request and it's argument
  1. It's result should be a router (this is not checked but will lead to error)
  1. This two routers are combined into bigger one.

Router is a very complicated monad so there is a big list of what it is
1. Router is a _Reader_: original request can be received with

  `test_maybe(&MonadRouter::Maybe.method(:pure))`
1. HTTPRequestRouter is a _State_: request path can be modified in a router and
  all next routers in chain will use new request path

  `pure.contramap_request_path { |path| new_path }`

1. HTTPRequestRouter is an _Alternative_: you can combine routers with `|`
  and first matched would be selected during exection.
  `pure` matches any request and `empty` fails to match

### Usage with Rack

```ruby
# Somewhere in config.ru
require 'monad_router/rack'
use MonadRouter::Rack::Middleware do
  msum(
    match('blog') >> get(/id/) >=
      ->(id) { pure([:article, id: Integer(id)]) },
    pure([:not_found]),
  )
end

# Somewhere in application
run(
  lambda do |env|
    # handle routing result
    make_handler(env[:route]) # env[:route] == [:article, id: 547].call
  end
)
```


## Development

After checking out the repo, run `bin/setup` to install dependencies.
You can also run `bin/console` for an interactive prompt that will allow you to experiment.

To install this gem onto your local machine, run `bundle exec rake install`.
To release a new version, update the version number in `version.rb`,
and then run `bundle exec rake release`,
which will create a git tag for the version, push git commits and tags,
and push the `.gem` file to [rubygems.org](https://rubygems.org).

## Contributing

Bug reports and merge requests are welcome on GitLab at
https://gitlab.com/JelF/monad_router.
This project is intended to be a safe, welcoming space for collaboration,
and contributors are expected to adhere to the
[Contributor Covenant](http://contributor-covenant.org) code of conduct.

## Code of Conduct

Everyone interacting in the MonadRouter project’s codebases, issue trackers,
chat rooms and mailing lists is expected to follow the
[code of conduct](https://gitlab.com/JelF/monad_router/blob/master/CODE_OF_CONDUCT.md).
