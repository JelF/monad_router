# frozen_string_literal: true

require 'bundler/setup'
require 'bundler/gem_tasks'

desc 'Run all automated checks'
task default: %i[lint test]

desc 'Run all linters (CI lint stage)'
task lint: %i[lint:rubocop lint:doc_coverage]

desc 'Run all test suites (only with current ruby version)'
task test: :'test:rspec'

require 'rubocop/rake_task'
RuboCop::RakeTask.new(:'lint:rubocop')

require 'rspec/core/rake_task'
RSpec::Core::RakeTask.new(:'test:rspec')

desc 'Run tests and open coverage report in browser'
task 'test:coverage:open': :'test:rspec' do
  require 'launchy'
  Launchy.open("file://#{File.expand_path('coverage/index.html', __dir__)}")
end

require 'yard'
YARD::Rake::YardocTask.new(:'doc:generate')

desc 'Re-generate documentation and open it in browser'
task 'doc:open': :'doc:generate' do
  require 'launchy'
  Launchy.open("file://#{File.expand_path('doc/frames.html', __dir__)}")
end

desc 'Find undocmunted code'
task 'lint:doc_coverage': :'doc:generate' do
  undocumented = YARD::Registry.select { |item| item.docstring.blank? }
  next if undocumented.empty?

  error_message = +'Failed to find documentation for following items:'
  undocumented.each { |item| error_message << "\n\t- #{item}" }
  abort error_message
end
