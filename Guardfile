# frozen_string_literal: true

guard :rubocop do
  watch(/.+\.rb$/)
  watch(%r{(?:.+/)?\.rubocop(?:_todo)?\.yml$}) { |m| File.dirname(m[0]) }
end

guard 'yard', server: false do
  watch(%r{lib\/.+\.rb})
end

guard :rspec, cmd: 'rspec', all_on_start: true do
  watch(%r{^spec/.+_spec\.rb$})
  watch(%r{^spec/support/.+\.rb$}) { 'spec' }
  watch(%r{^lib/monad_router/rack/(.+)\.rb$}) do
    'spec/monad_router/rack/middleware_spec.rb'
  end
  watch(%r{^lib/(.+)\.rb$}) do |match|
    subject = +'spec'
    match[1].split('/').map do |segment|
      "#{subject << '/' << segment}_spec.rb"
    end
  end
  watch('spec/spec_helper.rb') { 'spec' }
end
